Soluciones GBH - Frontend Test
==============================

Hello and thank you for taking the time to take this test.

The goal of this test is to see how you work and what you know. The test is simple, so that you can focus on showing your skills and knowledge

## Instructions

1. Clone this repository
2. Create a new `dev` branch
3. Place your code inside `./src/`
4. Do a pull request from the `dev` branch to the `master` branch.
5. Wait for our team to contact you.

When you finish, please do a pull request to `master` and write about your approach in the description. Our team will take some time to review your code and ask questions which you would need to answer.

Let us know if you run into any issues (report an issue in the git project).


## Requirements

We'd like you to implement a static site, it has 2 sections (home and "Get started"), it also has a side menu. Both sections must be responsive.

In the `./design/` folder you will find the .ai and .png that you will be implementing. Inside the `./design/assets/` folder, you will find icons and backgrounds that you'll need.

**Behaviours**

- Menu
  - Open when the menu icon is cliked
  - When it opens, it pushes content to the left (should have a sliding animation)

- Header
  - Secondary menu (seen in the second slide of the homepage) slides down when you reach this second slide and should be fixed to the top. When scrolling up, if you reach slide 1 it should slide up.
  
- Content
  - All content should be aligned to the center (vertically and horizontally)
  - Each slide should cover the screen


**Code must be in english**

## Evaluation

Must have:

- Valid HTML
- Valid CSS3
- Valid JS (If any)
- Your answers during review
- UTF8 file encoding
- Clean readable code
- Commented code

Nice to have (bonus points):
- An informative, detailed description in the PR
- Bootstrap or other frameworks (Should detail why you are using it)
- Setup script (if necessary)
- Using SASS
- Using BEM Convention
- Animations with CSS rather than JS
- A git history (even if brief) with clear, concise commit messages.


---

Good luck!